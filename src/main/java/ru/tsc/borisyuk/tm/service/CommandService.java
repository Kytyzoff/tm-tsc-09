package ru.tsc.borisyuk.tm.service;

import ru.tsc.borisyuk.tm.api.ICommandRepository;
import ru.tsc.borisyuk.tm.api.ICommandService;
import ru.tsc.borisyuk.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
