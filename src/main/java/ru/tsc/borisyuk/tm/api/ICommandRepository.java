package ru.tsc.borisyuk.tm.api;

import ru.tsc.borisyuk.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
